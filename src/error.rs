use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed to Parse URL")]
    Url(#[from] url::ParseError),
    #[error("Server returned invalid response")]
    ServerIsBroken,
    #[error("Failed to (de)serialize Json")]
    Json {
        source: serde_json::error::Error,
        json: String,
    },
    #[error("Http request failed")]
    Network(#[from] reqwest::Error),
    #[error("Invalid username or password")]
    InvalidLogin,
    #[error("Access denied")]
    AccessDenied,
    #[error("Input exceeds max size")]
    InputSize,
    #[error("Invalid cache response")]
    InvalidCaching,
}
