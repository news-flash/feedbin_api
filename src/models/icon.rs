use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Icon {
    pub host: String,
    pub url: String,
}
