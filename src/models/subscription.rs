use crate::{FeedID, SubscriptionID};
use serde_derive::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Deserialize)]
pub struct Subscription {
    pub id: SubscriptionID,
    pub created_at: String,
    pub feed_id: FeedID,
    pub title: String,
    pub feed_url: String,
    pub site_url: String,
}

#[derive(Debug, Serialize)]
pub struct CreateSubscriptionInput {
    pub feed_url: String,
}

#[derive(Debug, Deserialize)]
pub struct SubscriptionOption {
    pub feed_url: String,
    pub title: String,
}

#[derive(Debug)]
pub enum CreateSubscriptionResult {
    Created(Subscription),
    Found(Url),
    NotFound,
    MultipleOptions(Vec<SubscriptionOption>),
}

#[derive(Debug, Serialize)]
pub struct UpdateSubscriptionInput {
    pub title: String,
}

pub enum SubscriptionMode {
    Extended,
}

impl std::fmt::Display for SubscriptionMode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            SubscriptionMode::Extended => write!(f, "extended"),
        }
    }
}
