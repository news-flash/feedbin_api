use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Cache {
    pub etag: String,
    pub last_modified: String,
}

#[derive(Debug)]
pub struct CacheResult<T> {
    pub value: T,
    pub cache: Option<Cache>,
}

#[derive(Debug)]
pub enum CacheRequestResponse<T> {
    Modified(CacheResult<T>),
    NotModified,
}
